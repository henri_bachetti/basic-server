
#ifdef ESP32
#include <WiFi.h>
#include <WebServer.h>
#else
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#endif
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

const char *ssid = "AP-XXXXX";
const char *password = "..........";

const char* ntpServer = "pool.ntp.org";
time_t  bootTime;
char timeBuffer[80];
unsigned connections;

#define OLED_RESET     -1
#define SCREEN_ADDRESS 0x3C
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#ifdef ESP32
WebServer server(80);
//#define LED         LED_BUILTIN
//#define LED_CMD     LOW
#define LED         23
#define LED_CMD     HIGH
#else
ESP8266WebServer server(80);
#define LED         LED_BUILTIN
#define LED_CMD     LOW
#endif

void handleRoot() {
  int ledState = digitalRead(LED);
  String state = ledState == LED_CMD ? "ON" : "OFF";
  String message = "<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}";
  message += ".button { background-color: #4CAF50; border: none; color: white; padding: 16px 40px;";
  message += "text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}";
  message += ".button2 {background-color: #555555;}</style></head>";
#ifdef ESP32
  message += "<body><h1>ESP32 Web Server</h1>";
#else
  message += "<body><h1>ESP8266 Web Server</h1>";
#endif
  message += "<p>LED - State " + state + "</p>";
  if (ledState == LED_CMD) {
    message += "<p><a href=\"/off\"><button class=\"button button2\">OFF</button></a></p>";
  } else {
    message += "<p><a href=\"/on\"><button class=\"button\">ON</button></a></p>";
  }
  message += "<p>Boot : " + String(timeBuffer) + "</p>";
  message += "<p>WIFI Connections : " + String(connections) + "</p>";
  message += "<p>MAC : " + WiFi.BSSIDstr() + "</p>";
  message += "<p>RSSI : " + String(WiFi.RSSI()) + " dBm</p>";
  message += "</body></html>";
  server.send(200, "text/html", message);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
}

#ifdef ESP32
void onStationConnected(WiFiEvent_t event, WiFiEventInfo_t info) {
  Serial.println("Connected to AP successfully!");
}

void onStationDisconnected(WiFiEvent_t event, WiFiEventInfo_t info) {
  digitalWrite(LED, LED_CMD);
  Serial.println("Disconnected from WiFi access point");
  Serial.print("WiFi lost connection. Reason: ");
  Serial.println(info.disconnected.reason);
  Serial.printf("Trying to reconnect to %s\n", ssid);
  WiFi.reconnect();
}
void onStationGotIP(WiFiEvent_t event, WiFiEventInfo_t info) {
  digitalWrite(LED, !LED_CMD);
  Serial.printf("WiFi connected to %s\n", ssid);
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  connections++;
}
#else
void onStationConnected(const WiFiEventStationModeConnected& evt) {
  Serial.println("Connected to AP successfully!");
}

void onStationDisconnected(const WiFiEventStationModeDisconnected& evt) {
  Serial.print("Station disconnected: ");
  digitalWrite(LED, LED_CMD);
  Serial.println("Disconnected from WiFi access point");
  Serial.print("WiFi lost connection. Reason: ");
  Serial.println(evt.reason);
  Serial.printf("Trying to reconnect to %s\n", ssid);
  WiFi.reconnect();
}

void onStationGotIP(const WiFiEventStationModeGotIP& evt) {
  digitalWrite(LED, !LED_CMD);
  Serial.printf("WiFi connected to %s\n", ssid);
  Serial.print("IP address: "); Serial.println(WiFi.localIP());
  connections++;
}
WiFiEventHandler stationConnectedHandler;
WiFiEventHandler stationDisconnectedHandler;
WiFiEventHandler stationGotIpHandler;
#endif

void setup() {
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LED_CMD);
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
  }
#ifdef ESP32
  WiFi.onEvent(onStationConnected, SYSTEM_EVENT_STA_CONNECTED);
  WiFi.onEvent(onStationDisconnected, SYSTEM_EVENT_STA_DISCONNECTED);
  WiFi.onEvent(onStationGotIP, SYSTEM_EVENT_STA_GOT_IP);
#else
  stationConnectedHandler = WiFi.onStationModeConnected(onStationConnected);
  stationDisconnectedHandler = WiFi.onStationModeDisconnected(onStationDisconnected);
  stationGotIpHandler = WiFi.onStationModeGotIP(onStationGotIP);
#endif
  configTzTime("CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00", ntpServer);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.print("Connecting to ");
  Serial.println(ssid);

  server.on("/", handleRoot);

  server.on("/inline", []() {
    server.send(200, "text/plain", "This works as well");
  });

  server.on("/on", []() {
    Serial.println("ON");
    digitalWrite(LED, LED_CMD);
    handleRoot();
  });

  server.on("/off", []() {
    Serial.println("OFF");
    digitalWrite(LED, !LED_CMD);
    handleRoot();
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop() {
  static unsigned long lastTime;
  server.handleClient();
  if (millis() - lastTime >= 1000) {
    if (bootTime < 1000) {
      bootTime = time( NULL );
      struct tm *pTime = localtime(&bootTime);
      strftime(timeBuffer, 80, "%d/%m/%Y %H:%M", pTime);
      Serial.println(timeBuffer);
    }
    display.clearDisplay();
    display.setTextSize(1);      // Normal 1:1 pixel scale
    display.setTextColor(SSD1306_WHITE); // Draw white text
    display.setCursor(0, 0);
    display.print("WIFI ") ; display.print(WiFi.isConnected() ? "CONNECTED" : "DISCONNECTED");
    display.print(" ("); display.print(connections); display.print(")");
    display.setCursor(0, 20);
    display.print("Boot "); display.print(timeBuffer);
    display.setCursor(0, 32);
    display.print("MAC ") ; display.print(WiFi.BSSIDstr());
    display.setCursor(0, 44);
    display.print("IP "); display.print(WiFi.localIP());
    display.setCursor(0, 56);
    display.print("RSSI "); display.print(WiFi.RSSI()); display.print(" dBm");
    display.display();
    lastTime = millis();
  }
}
